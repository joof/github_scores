import json
import asyncio
import aiohttp
from unittest import mock
from logging import Logger
from typing import Optional
from datetime import timedelta

from asgiref.sync import sync_to_async
from django.http import Http404
from django.urls import reverse
from django.test import TestCase
from django.utils import timezone

from aioresponses import aioresponses

from .utils import RetryMixin
from .views import RepositoryItem
from .models import RepoTypes, Repository
from .backends import RepoFetcher, GithubRepoFetcher


class RepositoryViewTestCase(TestCase):
    def setUp(self) -> None:
        self.stars = [200, 300, 400, 399, 100]
        self.forks = [200, 50, 50, 50, 200]
        self.scores = [600, 400, 500, 499, 500]
        self.is_popular = [True, False, True, False, True]
        self.base_url = "https://github.com"
        self.repos = [
            Repository(url=f"{self.base_url}/0", pending=True),
            Repository(url=f"{self.base_url}/1", pending=False,
                       last_updated=timezone.now() - (2 * Repository.DEPRECATION_PERIOD)),
        ]
        base_len = len(self.repos)
        for index, (stars, forks) in enumerate(zip(self.stars[base_len:],
                                                   self.forks[base_len:])):
            url_index = index + base_len
            url = f"{self.base_url}/{url_index}"
            self.repos.append(Repository(url=url,
                                         pending=False,
                                         last_updated=timezone.now(),
                                         stars=stars, forks=forks))
        Repository.objects.bulk_create(self.repos)

        self.non_repo_calls = 0
        self.non_repo = Repository(pending=False, stars=500, forks=500)

        ##
        def fetch_repo(url: str) -> Optional[Repository]:
            return Repository.objects.filter(url=url).first()

        async def update_repo(base_repo: Repository) -> Repository:
            repo = await sync_to_async(fetch_repo)(base_repo.url)
            if repo is None:
                self.non_repo_calls += 1
                self.non_repo.url = base_repo.url
                self.non_repo.last_updated = timezone.now()
                await sync_to_async(self.non_repo.save)()
                return self.non_repo
            repo.pending = False
            repo.last_updated = timezone.now()
            repo_index = None
            for i, r in enumerate(self.repos):
                if repo.url == r.url:
                    repo_index = i
            self.assertIsNotNone(repo_index)
            repo.stars = self.stars[repo_index]
            repo.forks = self.forks[repo_index]
            await sync_to_async(repo.save)()
            return repo
        self.repo_fetcher = mock.AsyncMock()
        self.repo_fetcher.fetch_repository.side_effect = update_repo

    def test_repos(self) -> None:
        path = reverse("repository:repository-item")
        with mock.patch('repository.views.RepoFetcher', self.repo_fetcher):
            for i, repo in enumerate(self.repos):
                resp = self.client.get(f"{path}?link={repo.url}")
                resp_json = resp.json()
                self.assertEqual(resp_json['stars'], self.stars[i])
                self.assertEqual(resp_json['forks'], self.forks[i])
                self.assertEqual(resp_json['score'], self.scores[i])
                self.assertEqual(resp_json['is_popular'], self.is_popular[i])
            # test repo with no database instance
            self.assertEqual(self.non_repo_calls, 0)
            resp = self.client.get(f"{path}?link={self.non_repo.url}")
            resp_json = resp.json()
            self.assertEqual(resp_json['stars'], self.non_repo.stars)
            self.assertEqual(resp_json['forks'], self.non_repo.forks)
            self.assertEqual(resp_json['score'], self.non_repo.score)
            self.assertEqual(resp_json['is_popular'], self.non_repo.is_popular)
            self.assertEqual(self.non_repo_calls, 1)

    def test_errors(self) -> None:
        path = reverse("repository:repository-item")
        resp = self.client.get(path=path)
        self.assertEqual(resp.status_code, 400)
        with mock.patch('repository.views.RepoFetcher', new_callable=mock.AsyncMock) as fetcher:
            fetcher.fetch_repository.side_effect = [aiohttp.ClientError(), asyncio.TimeoutError(), Exception()]
            link = "https://github.com/test-error/"
            resp = self.client.get(path=f"{path}?link={link}")
            self.assertEqual(resp.status_code, 503)
            self.assertEqual(resp.headers["Retry-After"], str(RepositoryItem.RETRY_AFTER))
            resp = self.client.get(path=f"{path}?link={link}")
            self.assertEqual(resp.status_code, 503)
            self.assertEqual(resp.headers["Retry-After"], str(RepositoryItem.RETRY_AFTER))

    def test_url(self) -> None:
        async def mock_fetcher(repo: Repository) -> Repository:
            return Repository(url=repo.url, pending=False, last_updated=timezone.now(), stars=400, forks=400)
        path = reverse("repository:repository-item")
        expected_urls = [("https://github.com/00", "https://github.com/00"),
                         ("http://github.com/11", "https://github.com/11"),
                         ("github.com/22", "https://github.com/22"),
                         ("https://github.com/33/", "https://github.com/33"),
                         ("http://github.com/44/", "https://github.com/44"),
                         ("github.com/55/", "https://github.com/55"), ]
        with mock.patch('repository.views.RepoFetcher', new_callable=mock.AsyncMock) as fetcher:
            fetcher.fetch_repository.side_effect = mock_fetcher
            for url, expected in expected_urls:
                resp = self.client.get(f"{path}?link={url}")
                value = resp.json()["url"]
                self.assertEqual(value, expected)


class RepositoryModelTestCase(TestCase):
    def test_type(self) -> None:
        supported_repos = [
            ("https://github.com/zalando/patroni", RepoTypes.GITHUB),
            ("https://github.com/hashicorp/consul", RepoTypes.GITHUB),
        ]
        non_supported_repos = [
            "https://gitlab.com/gitlab-org/gitlab",
            "github.com/zalando/patroni",
            "google.com",
        ]
        for url, repo_type in supported_repos:
            repo = Repository(url=url)
            self.assertEqual(repo.type(), repo_type)
        for url in non_supported_repos:
            repo = Repository(url=url)
            with self.assertRaises(ValueError):
                _ = repo.type()

    def test_deprecation(self) -> None:
        repos = [
            # repo object - should be considered as deprecated
            (Repository(pending=True, last_updated=timezone.now()), True),
            (Repository(pending=False, last_updated=None), True),
            (Repository(pending=False, last_updated=timezone.now()), False),
            (Repository(pending=False,
                        last_updated=timezone.now() - Repository.DEPRECATION_PERIOD - timedelta(seconds=3)),
             True),
            (Repository(pending=False,
                        last_updated=timezone.now() - Repository.DEPRECATION_PERIOD + timedelta(seconds=3)),
             False),
        ]
        for repo, deprecated in repos:
            self.assertEqual(repo.deprecated, deprecated)

    def test_popularity(self) -> None:
        repos = [
            # repo object - expected score - popularity
            (Repository(stars=10, forks=20), 50, False),
            (Repository(stars=150, forks=150), 450, False),
            (Repository(stars=199, forks=150), 499, False),
            (Repository(stars=100, forks=200), 500, True),
            (Repository(stars=101, forks=200), 501, True),
            (Repository(stars=200, forks=200), 600, True),
        ]
        for repo, score, is_popular in repos:
            self.assertEqual(repo.score, score)
            self.assertEqual(repo.is_popular, is_popular)
        exc_repos = [
            Repository(stars=1000),
            Repository(forks=1000),
        ]
        for repo in exc_repos:
            with self.assertRaises(ValueError):
                _ = repo.score
            with self.assertRaises(ValueError):
                _ = repo.is_popular


class GithubRepoFetcherTestCase(TestCase):
    @aioresponses()
    async def test_get(self, mocked) -> None:
        url = "https://github.com/zalando/patroni"
        repo = Repository(url=url)
        fetcher = GithubRepoFetcher(repo)
        body = {"stargazers_count": 200, "forks": 250}
        mocked.get(fetcher.api_url, status=200, body=json.dumps(body))
        fetched_repo = await fetcher.retry_run()
        self.assertEqual(fetched_repo.stars, 200)
        self.assertEqual(fetched_repo.forks, 250)

    @aioresponses()
    async def test_error(self, mocked) -> None:
        url = "https://github.com/zalando/patroni"
        repo = Repository(url=url)
        fetcher = GithubRepoFetcher(repo)
        # 404 not found
        mocked.get(fetcher.api_url, status=404)
        with self.assertRaises(Http404):
            _ = await fetcher.retry_run()
        # unknown error
        mocked.get(fetcher.api_url, status=500)
        with self.assertRaises(Exception):
            _ = await fetcher.retry_run()
        # no api token
        fetcher.api_token = None
        body = {"stargazers_count": 200, "forks": 250}
        mocked.get(fetcher.api_url, status=200, body=json.dumps(body))
        with self.assertLogs(fetcher.logger, level="WARNING") as logger:
            _ = await fetcher.retry_run()
            self.assertEqual(len(logger.output), 1)


class RepoFetcherTestCase(TestCase):
    async def test_save(self) -> None:
        repo = Repository(url="https://github.com/zalando/patroni")
        before = timezone.now()
        fetched_repo = await RepoFetcher.fetch_repository(repo)
        after = timezone.now()
        self.assertIsNotNone(fetched_repo.pk)
        self.assertEqual(fetched_repo.url, repo.url)
        self.assertGreaterEqual(fetched_repo.last_updated, before)
        self.assertLessEqual(fetched_repo.last_updated, after)
        self.assertFalse(fetched_repo.deprecated)

    async def test_fetch_error(self) -> None:
        repo = Repository(url="https://gitlab.com/gitlab-org/gitlab")
        with self.assertRaises(ValueError):
            _ = await RepoFetcher.fetch_repository(repo)
        repo = Repository(url="https://github.com/zalando/patroni")
        with mock.patch("repository.backends.GithubRepoFetcher", new_callable=mock.MagicMock) as github_fetcher:
            github_fetcher.retry_run = mock.AsyncMock()
            github_fetcher.retry_run.return_value = None  # any non-repository object
            with self.assertRaises(Exception):
                _ = await RepoFetcher.fetch_repository(repo)


class RetryMixinTestCase(TestCase):

    class MockRetry(RetryMixin):
        RETRY_COUNT_MAX = 4
        RETRY_DELAY_BASE = 0.1
        RETRY_DELAY_MIN = 0.1
        RETRY_DELAY_MAX = 0.2
        RETRY_ON_EXCEPTIONS = [ValueError, asyncio.TimeoutError, ]
        #
        counter = -1
        logger = Logger("test.repository")

        async def run(self, *args, **kwargs):
            self.counter += 1
            if self.counter < len(self.RETRY_ON_EXCEPTIONS):
                raise self.RETRY_ON_EXCEPTIONS[self.counter]()
            if kwargs.get("exception"):
                raise ImportError()
            if kwargs.get("retry"):
                raise self.RETRY_ON_EXCEPTIONS[0]()
            return self.counter

    async def test_retry(self) -> None:
        # successful retry
        obj = RetryMixinTestCase.MockRetry()
        with self.assertLogs(obj.logger, level="ERROR") as logger:
            result = await obj.retry_run()
            self.assertEqual(obj.counter, len(obj.RETRY_ON_EXCEPTIONS))
            self.assertEqual(result, obj.counter)
            self.assertEqual(len(logger.output), len(obj.RETRY_ON_EXCEPTIONS))
        # exhaust on retry
        obj = RetryMixinTestCase.MockRetry()
        with self.assertLogs(obj.logger, level="ERROR") as logger:
            with self.assertRaises(obj.RETRY_ON_EXCEPTIONS[0]):
                _ = await obj.retry_run(retry=True)
            self.assertEqual(obj.counter + 1, obj.RETRY_COUNT_MAX)
            self.assertEqual(len(logger.output), obj.RETRY_COUNT_MAX)
        # unhandled exception
        obj = RetryMixinTestCase.MockRetry()
        with self.assertLogs(obj.logger, level="ERROR") as logger:
            with self.assertRaises(ImportError):
                _ = await obj.retry_run(exception=True)
            self.assertEqual(obj.counter, len(obj.RETRY_ON_EXCEPTIONS))
            self.assertEqual(len(logger.output), len(obj.RETRY_ON_EXCEPTIONS) + 1)
