from enum import Enum
from urllib.parse import urlparse
from datetime import timedelta

from django.utils import timezone
from django.db import models
from django.contrib.postgres.indexes import HashIndex


class RepoTypes(Enum):
    GITHUB = "github.com"
    UNSUPPORTED = "unsupported type"


class Repository(models.Model):
    url = models.URLField(unique=True)
    pending = models.BooleanField(default=True)
    stars = models.PositiveIntegerField(null=True, blank=True)
    forks = models.PositiveIntegerField(null=True, blank=True)
    last_updated = models.DateTimeField(null=True, blank=True)

    class Meta:
        indexes = (HashIndex(fields=('url',)),)

    SCORE_THRESHOLD = 500
    DEPRECATION_PERIOD = timedelta(days=1)

    @property
    def score(self) -> int:
        """
        Calculate git repository score to determine whether a repo is famous or not
        """
        if self.forks is None or self.stars is None:
            raise ValueError(f"Cant calculate score for repository {self}")
        return self.forks * 2 + self.stars

    @property
    def is_popular(self) -> bool:
        """
        :return: Whether a repo is popular or not
        """
        return self.score >= self.SCORE_THRESHOLD

    @property
    def deprecated(self) -> bool:
        """
        :return: Whether you can rely on the cached result or need to fetch the data again
        """
        if self.pending:
            return True
        if self.last_updated is None:
            return True
        return self.last_updated + self.DEPRECATION_PERIOD < timezone.now()

    def type(self, raise_exception=True) -> RepoTypes:
        netloc = urlparse(self.url).netloc
        try:
            return RepoTypes(netloc)
        except ValueError:
            if raise_exception:
                raise ValueError(f"Repository {netloc} for url {self.url} is not yet supported!")
        return RepoTypes.UNSUPPORTED

    def __str__(self) -> str:
        return f"repository: {self.type(raise_exception=False)} " \
               f"- stars: {self.stars} " \
               f"- forks: {self.forks} " \
               f"- last update: {self.last_updated}. " \
               f"- url: {self.url}"
