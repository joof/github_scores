import logging
import os
import aiohttp
import asyncio
from http import HTTPStatus
from abc import abstractmethod
from urllib.parse import urlparse
from typing import Optional, Dict

from asgiref.sync import sync_to_async
from django.http import Http404
from django.utils import timezone

from .utils import RetryMixin
from .models import RepoTypes, Repository


logger = logging.getLogger("github_scores.repository")


class AbstractRepoFetcher(RetryMixin):
    def __init__(self, repo: Repository):
        self.repo = repo
        self.logger = logger
        self.RETRY_ON_EXCEPTIONS = [aiohttp.ClientError,
                                    asyncio.TimeoutError,
                                    OSError, ]
        self.setup()

    def setup(self) -> None:
        pass

    async def run(self):
        return await self.fetch_repo()

    @abstractmethod
    async def fetch_repo(self) -> Repository:
        pass


class GithubRepoFetcher(AbstractRepoFetcher):
    API_BASE_URL = "https://api.github.com/repos"
    api_url: str
    api_token = os.environ.get("GITHUB_API_TOKEN")

    def setup(self) -> None:
        repo_name = urlparse(self.repo.url).path
        self.api_url = self.API_BASE_URL + repo_name

    async def fetch_repo(self) -> Repository:
        headers: Optional[Dict[str, str]] = {"Authorization": f"Token {self.api_token}"}
        if self.api_token is None:
            headers = None
            self.logger.warning("$GITHUB_API_TOKEN is not set! "
                                "Please set the api token so the system wouldn't reach api limit")
        try:
            async with aiohttp.ClientSession(headers=headers) as session:
                async with session.get(self.api_url) as response:
                    json_body = await response.json()
                    response.raise_for_status()
        except aiohttp.ClientResponseError as e:
            if e.status == HTTPStatus.NOT_FOUND:
                raise Http404(f"#{self.repo.url} is not a valid github url")
            raise Exception(f"Github response for url #{self.repo.url} "
                            f"with api url #{self.api_url} not successful. "
                            f"response: #{response.status}: {json_body}")
        self.repo.stars = json_body["stargazers_count"]
        self.repo.forks = json_body["forks"]
        return self.repo


class RepoFetcher:
    @staticmethod
    async def fetch_repository(repository: Repository) -> Repository:
        repo_type = repository.type()
        fetched_repo = None
        if repo_type == RepoTypes.GITHUB:
            fetched_repo = await GithubRepoFetcher(repository).retry_run()

        if isinstance(fetched_repo, Repository):
            fetched_repo.pending = False
            fetched_repo.last_updated = timezone.now()
            await sync_to_async(fetched_repo.save)()
            return fetched_repo

        # should never reach here!
        logger.exception(f"{fetched_repo} is not a valid repository object!")
        raise Exception(f"{fetched_repo} is not a valid repository object!")
