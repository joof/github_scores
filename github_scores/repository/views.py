import asyncio
import aiohttp
from asgiref.sync import sync_to_async
from django.views.generic import View
from django.http.request import HttpRequest
from django.http.response import JsonResponse, HttpResponse
from django.utils.decorators import classonlymethod

from .models import Repository
from .serializers import RepositorySerializer
from .backends import RepoFetcher
from .utils import clean_url


class RepositoryItem(View):
    RETRY_AFTER = 300    # seconds

    @classonlymethod
    def as_view(cls, **initkwargs):
        view = super().as_view(**initkwargs)
        view._is_coroutine = asyncio.coroutines._is_coroutine   # type: ignore
        return view

    def get_repo(self, url: str):
        return Repository.objects.filter(url=url).first()

    async def get(self, request: HttpRequest):
        uncleaned_url = request.GET.get("link")
        if uncleaned_url is None:
            return HttpResponse("No link is provided with the request!", status=400)
        url = clean_url(uncleaned_url)
        repository = await sync_to_async(self.get_repo)(url)
        if repository is None:
            repository = Repository(url=url)
        if repository.deprecated:
            try:
                repository = await RepoFetcher.fetch_repository(repository)
            except ValueError as e:
                return HttpResponse(str(e), status=400)
            except (aiohttp.ClientError, asyncio.TimeoutError):
                return HttpResponse("Connection error to repository endpoint. "
                                    "Please try again later",
                                    status=503,
                                    headers={"Retry-After": self.RETRY_AFTER})
        data = RepositorySerializer(repository).data
        return JsonResponse(data)
