from rest_framework.serializers import ModelSerializer

from .models import Repository


class RepositorySerializer(ModelSerializer):
    class Meta:
        model = Repository
        fields = ('url', 'stars', 'forks', 'last_updated', 'score', 'is_popular')
        read_only_fields = fields
