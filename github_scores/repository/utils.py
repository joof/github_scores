import asyncio
from logging import Logger
from abc import ABC, abstractmethod
from typing import List, Type, Any


def clean_url(url: str) -> str:
    if url.endswith("/"):
        url = url[:-1]
    url = url.replace("http://", "https://", 1)
    if not url.startswith("https://"):
        url = f"https://{url}"
    return url


class RetryMixin(ABC):
    RETRY_COUNT_MAX: int = 3
    # in seconds
    RETRY_DELAY_BASE: float = 2
    RETRY_DELAY_MIN: float = 3
    RETRY_DELAY_MAX: float = 16
    RETRY_ON_EXCEPTIONS: List[Type[Exception]] = []
    logger: Logger

    @abstractmethod
    async def run(self, *args, **kwargs):
        pass

    async def retry_run(self, *args, **kwargs) -> Any:
        for retry_count in range(1, self.RETRY_COUNT_MAX + 1):
            try:
                return await self.run(*args, **kwargs)
            except (*self.RETRY_ON_EXCEPTIONS,) as e:
                self.logger.error(f'Failed attempt #{retry_count}: class #{type(self)} '
                                  f'with args #{args} and kwargs #{kwargs}. '
                                  f'Exception: #{repr(e)}')
                if retry_count >= self.RETRY_COUNT_MAX:
                    raise
                delay = self.RETRY_DELAY_MIN + self.RETRY_DELAY_BASE * (2 ** retry_count)
                delay = min(delay, self.RETRY_DELAY_MAX)
                self.logger.info(f"Retrying in {delay} seconds ...")
                await asyncio.sleep(delay)
            except Exception as e:
                self.logger.exception(f"Unhandled error: {repr(e)}")
                raise
