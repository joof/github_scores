from django.urls import path

from .views import RepositoryItem


app_name = "repository"


urlpatterns = [
    path('', RepositoryItem.as_view(), name="repository-item"),     # repo url is passed via query-string
]
