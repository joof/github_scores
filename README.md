# Github Scores
Github scores is a django project to check whether a github repository is popular or not.


## Made Assumptions
Since the popularity of github repositories barely change throughout the day, I used database to cache results for 24 hours.

The system is designed in a way that adding new providers would be easy with no need to break other parts of the project.

I also considered that for some reason the endpoint might not be available and added a RetryMixin that retries the request in that case.



## Tech stack
For this project I used django version 3.2 with async views, so workers can respond to other requests while fetching data from github!

The database is postgresql 14.0.

The runtime environment is Docker. I used multistage dockerfile to reduce the image size as much as possible.

Also important libraries that I used are:
* uvicorn: to run django in async mode
* django-health-check: to provide system healthcheck
* whitenoise: to serve static files while using uvicorn

## Setup Project
### Database
Set environment file: `docker/dev/db/.env`.

You can use `docker/dev/db/.env.sample` file to create your own `.env`


### Application
Set environment file: `docker/dev/django/.env`.

You can use `docker/dev/django/.env.sample` file to create your own `.env`


### Compose
1. Set environment file: `dev.compose.env`. You can use `dev.compose.env.sample` sample file.


2. Build images and create containers in the detached mode:
* `docker-compose -f dev.compose.yml --env-file dev.compose.env up --build -d`


## To connect to the service and run tests:
1. `docker exec -it django bash`
2. `flake8`
3. `mypy .`
4. `python manage.py test`


## Further Improvements
1. Use redis (or any cache database) instead of postgres to cache the results and use postgres as main db and gather info about the repository.
2. Add more features like: support for other repositories, authentication and more statistics about repositories.
3. Add background process to prefetch branches that are accessed the most via users.
4. Targeting branches with lots of change throughout the day and decrease cache time for them.
5. Add docker for stage and production mode, pre-commit hooks and CI/CD.
